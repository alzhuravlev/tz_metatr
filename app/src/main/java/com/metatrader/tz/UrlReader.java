package com.metatrader.tz;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class UrlReader {

    public interface Listener {
        void onDataReceived(byte[] bytes, int len);
    }

    private String urlString;
    private Listener listener;

    public UrlReader(@NonNull String urlString, @NonNull Listener listener) {
        this.urlString = urlString;
        this.listener = listener;
    }

    public void process() throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setInstanceFollowRedirects(false);
        try (InputStream inputStream = connection.getInputStream()) {
            byte[] buf = new byte[8192];
            while (true) {
                int len = inputStream.read(buf);
                if (len == -1)
                    break;
                if (len == 0)
                    continue;
                listener.onDataReceived(buf, len);
            }
        }
        // finalize line processing
        listener.onDataReceived(new byte[]{'\n'}, 1);
    }
}
