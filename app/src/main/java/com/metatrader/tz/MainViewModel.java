package com.metatrader.tz;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;

public class MainViewModel extends AndroidViewModel {

    private final MutableLiveData<Boolean> processingActive = new MutableLiveData<>();
    private final MutableLiveData<Integer> processDataChanged = new MutableLiveData<>();
    private final MutableLiveData<Exception> error = new MutableLiveData<>();

    private final Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case MainService.MSG_START:
                    processStart();
                    break;
                case MainService.MSG_FINISH:
                    processFinish();
                    break;
                case MainService.MSG_ERROR:
                    processError((Exception) msg.obj);
                    break;
                case MainService.MSG_STRING_RECEIVED:
                    processStringReceived((String) msg.obj);
                    break;
            }
        }
    };

    private List<String> processData = new ArrayList<>();
    private List<String> processDataReadOnly = Collections.unmodifiableList(processData);

    private Future<Object> processFuture;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    protected void onCleared() {
        if (processFuture != null)
            processFuture.cancel(true);
    }

    public LiveData<Boolean> getProcessingActive() {
        return processingActive;
    }

    public LiveData<Integer> getProcessDataChanged() {
        return processDataChanged;
    }

    public LiveData<Exception> getError() {
        return error;
    }

    private void processStart() {
        processingActive.postValue(true);
    }

    private void processFinish() {
        processingActive.postValue(false);
    }

    private void processError(Exception e) {
        error.postValue(e);
    }

    private void processStringReceived(String s) {
        processData.add(s);
        processDataChanged.postValue(processData.size());
    }

    public List<String> getProcessData() {
        return processDataReadOnly;
    }

    public void process(String url, String mask) {
        processData.clear();
        processDataChanged.postValue(processData.size());
        processFuture = MainService.getInstance().process(getApplication(), url, mask, handler);
    }
}
