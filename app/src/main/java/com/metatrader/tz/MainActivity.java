package com.metatrader.tz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;
import androidx.lifecycle.ViewModelProvider;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements HasDefaultViewModelProviderFactory {

    private EditText editTextUrl;
    private EditText editTextMask;
    private TextView textViewStatus;
    private ProgressBar progressBar;
    private ListView listView;
    private Button processButton;
    private Button copyButton;

    private Adapter adapter;

    private MainViewModel mainViewModel;

    private Set<Integer> selectedPositions = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.editTextUrl = findViewById(R.id.edit_text_url);
        this.editTextMask = findViewById(R.id.edit_text_mask);
        this.textViewStatus = findViewById(R.id.text_view_status);
        this.progressBar = findViewById(R.id.progress_bar);
        this.listView = findViewById(R.id.list_view);
        this.processButton = findViewById(R.id.process_button);
        this.copyButton = findViewById(R.id.copy_button);

        copyButton.setOnClickListener(view -> copySelected());
        processButton.setOnClickListener(view -> process());

        progressBar.setVisibility(View.GONE);
        copyButton.setVisibility(View.GONE);

        editTextUrl.setText("https://www.ietf.org/rfc/rfc2626.txt");
        editTextMask.setText("*http://*");

        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        mainViewModel.getProcessingActive().observe(this, processingActive -> {
            if (processingActive) {
                progressBar.setVisibility(View.VISIBLE);
                processButton.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.GONE);
                processButton.setVisibility(View.VISIBLE);
            }
        });

        mainViewModel.getProcessDataChanged().observe(this, count -> {
            selectedPositions.clear();
            adapter.notifyDataSetChanged();
            textViewStatus.setText("Found: " + count);
        });

        mainViewModel.getError().observe(this, e -> {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        });

        adapter = new Adapter();
        listView.setAdapter(adapter);
    }

    private void process() {
        mainViewModel.process(editTextUrl.getText().toString(), editTextMask.getText().toString());
    }

    @NonNull
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        return ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication());
    }

    private class Adapter extends BaseAdapter {

        private final LayoutInflater inflater = LayoutInflater.from(MainActivity.this);

        @Override
        public int getCount() {
            return mainViewModel.getProcessData().size();
        }

        @Override
        public String getItem(int position) {
            String s = mainViewModel.getProcessData().get(position);
            return s.substring(0, Math.min(s.length(), 500));
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view;
            if (convertView == null) {
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }
            final TextView textView = (TextView) view;
            if (selectedPositions.contains(position))
                textView.setBackgroundColor(0x110000ff);
            else
                textView.setBackgroundColor(0x00000000);
            textView.setOnClickListener(v -> {
                if (selectedPositions.contains(position))
                    selectedPositions.remove(position);
                else
                    selectedPositions.add(position);
                copyButton.setVisibility(selectedPositions.isEmpty() ? View.GONE : View.VISIBLE);
                notifyDataSetChanged();
            });
            textView.setMaxLines(4);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            textView.setText(getItem(position));
            return view;
        }
    }

    private void copySelected() {
        List<String> strings = mainViewModel.getProcessData();
        StringBuilder sb = new StringBuilder();
        for (Integer idx : selectedPositions)
            if (idx < strings.size())
                sb.append(strings.get(idx)).append("\n");

        if (sb.length() == 0)
            return;

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Search results", sb.toString());
        clipboard.setPrimaryClip(clip);

        Toast.makeText(this, selectedPositions.size() + " strings copied to clipboard", Toast.LENGTH_LONG).show();
    }
}