package com.metatrader.tz;

public class LogReader {

    static {
        System.loadLibrary("tz");
    }

    private static native long n_CLogReader();

    private static native void n_delete(long ptr);

    private static native void n_SetFilter(long ptr, String filter);

    private static native void n_AddSourceBlock(long ptr, byte[] bytes, int len);

    private static native void n_finish(long ptr);

    private static native String[] n_consumeResults(long ptr);

    private final long ptr;

    public LogReader() {
        ptr = n_CLogReader();
    }

    @Override
    protected void finalize() throws Throwable {
        n_delete(ptr);
        super.finalize();
    }

    public void setFilter(String filter) {
        n_SetFilter(ptr, filter);
    }

    public void addSourceBlock(byte[] bytes, int len) {
        n_AddSourceBlock(ptr, bytes, len);
    }

    public void addSourceBlock(String s) {
        byte[] b = s.getBytes();
        addSourceBlock(b, b.length);
    }

    public void addSourceBlockNewLine(String s) {
        s = s + "\n";
        byte[] b = s.getBytes();
        addSourceBlock(b, b.length);
    }

    public void finish() {
        n_finish(ptr);
    }

    public String[] consumeResults() {
        return n_consumeResults(ptr);
    }
}
