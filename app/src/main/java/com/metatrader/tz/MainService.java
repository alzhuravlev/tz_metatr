package com.metatrader.tz;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class MainService {

    private static MainService instance;

    public static final int MSG_START = 0;
    public static final int MSG_FINISH = 1;
    public static final int MSG_ERROR = 2;
    public static final int MSG_STRING_RECEIVED = 3;

    public static MainService getInstance() {
        if (instance == null)
            instance = new MainService();
        return instance;
    }

    private final Executor executor = Executors.newSingleThreadExecutor();

    private static class Processor implements Callable<Object> {
        private Context context;
        private String url;
        private String mask;
        private Handler handler;

        public Processor(Context context, String url, String mask, Handler handler) {
            this.context = context;
            this.url = url;
            this.mask = mask;
            this.handler = handler;
        }

        @Override
        public Object call() throws Exception {
            File file = new File(context.getExternalFilesDir(null), "log.txt");
            try (Writer writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file))))) {
                handler.sendEmptyMessage(MSG_START);
                try {
                    LogReader logReader = new LogReader();
                    logReader.setFilter(mask);
                    UrlReader urlReader = new UrlReader(url, (bytes, len) -> {
                        logReader.addSourceBlock(bytes, len);
                        for (String s : logReader.consumeResults()) {
                            handler.sendMessage(handler.obtainMessage(MSG_STRING_RECEIVED, s));
                            try {
                                writer.write(s+"\n");
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                    urlReader.process();
                } catch (Exception e) {
                    handler.sendMessage(handler.obtainMessage(MSG_ERROR, e));
                }
                handler.sendEmptyMessage(MSG_FINISH);
            }
            return null;
        }
    }

    public Future<Object> process(Context context, String url, String mask, Handler handler) {
        FutureTask<Object> future = new FutureTask<>(new Processor(context, url, mask, handler));
        executor.execute(future);
        return future;
    }
}
