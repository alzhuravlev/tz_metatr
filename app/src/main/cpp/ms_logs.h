//
// Created by crane2002 on 5/3/2020.
//

#ifndef MINESWEEPERCOLORING_MS_LOGS_H
#define MINESWEEPERCOLORING_MS_LOGS_H

#define LOGI(...) ((void)__logi(__VA_ARGS__))
#define LOGW(...) ((void)__logw(__VA_ARGS__))
#define LOGE(...) ((void)__loge(__VA_ARGS__))
#define LOGD(...) ((void)__logd(__VA_ARGS__))

void __logi(const char *fmt, ...);

void __loge(const char *fmt, ...);

void __logw(const char *fmt, ...);

void __logd(const char *fmt, ...);

#endif //MINESWEEPERCOLORING_MS_LOGS_H
