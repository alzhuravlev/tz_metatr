//
// Created by crane on 10/20/2020.
//

#include <jni.h>
#include "CLogReader.h"
#include "jni_global.h"

using namespace std;

extern "C"
JNIEXPORT jlong JNICALL
Java_com_metatrader_tz_LogReader_n_1CLogReader(JNIEnv *env, jclass clazz) {
    return (jlong) new CLogReader();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_metatrader_tz_LogReader_n_1delete(JNIEnv *env, jclass clazz, jlong ptr) {
    delete (CLogReader *) ptr;
}

extern "C"
JNIEXPORT void JNICALL
Java_com_metatrader_tz_LogReader_n_1SetFilter(JNIEnv *env, jclass clazz, jlong ptr,
                                              jstring filter) {
    const char *c = env->GetStringUTFChars(filter, JNI_FALSE);
    ((CLogReader *) ptr)->SetFilter(c);
    env->ReleaseStringUTFChars(filter, c);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_metatrader_tz_LogReader_n_1AddSourceBlock(JNIEnv *env, jclass clazz, jlong ptr,
                                                   jbyteArray bytes, jint len) {
    jbyte *c = env->GetByteArrayElements(bytes, JNI_FALSE);
    ((CLogReader *) ptr)->AddSourceBlock((char *) c, len);
    env->ReleaseByteArrayElements(bytes, c, 0);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_metatrader_tz_LogReader_n_1finish(JNIEnv *env, jclass clazz, jlong ptr) {
    ((CLogReader *) ptr)->finish();
}

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_metatrader_tz_LogReader_n_1consumeResults(JNIEnv *env, jclass clazz, jlong ptr) {

    tzvector<tzstring> out;
    ((CLogReader *) ptr)->consumeResults(out);

    jobjectArray res = env->NewObjectArray(out.size(), env->FindClass("java/lang/String"),
                                           env->NewStringUTF(""));

    for (int i = 0; i < out.size(); i++)
        env->SetObjectArrayElement(res, i, env->NewStringUTF(out[i].c_str()));

    return res;
}