//
// Created by crane on 10/20/2020.
//
#include "CLogReader.h"
#include "tzvector.h"

bool CLogReader::SetFilter(const char *filter) {
//    LOGD("SetFilter(\"%s\")", filter);
    matcher.parse(filter);
    return true;
}

bool CLogReader::AddSourceBlock(const char *block, const size_t block_size) {
    for (int i = 0; i < block_size; i++) {
        const char &c = block[i];
        if (c == '\n' || c == '\r')
            finish();
        else
            _buf += c;
    }
    return true;
}

CLogReader::CLogReader() {
//    LOGD("CLogReader()");
}

CLogReader::~CLogReader() {
    // LOGD("~CLogReader()");
}

void CLogReader::consumeResults(tzvector<tzstring> &out) {
    if (_results.empty())
        return;
    for (tzstring &s: _results)
        out.emplace_back(s);
    _results.clear();
}

void CLogReader::finish() {
    if (!_buf.empty()) {
        if (_matchString(_buf))
            _results.emplace_back(_buf);
        _buf.clear();
    }
}

bool CLogReader::_matchString(const tzstring &s) {
    return matcher.matchString(s);
}


