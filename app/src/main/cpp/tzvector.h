//
// Created by crane on 11/5/2020.
//

#ifndef TZ_TZVECTOR_H
#define TZ_TZVECTOR_H

#include <cstddef>

//#include <vector>

//template<typename T> using tzvector = std::vector<T>;

template<class T>
class tzvector {
public:

    typedef T *iterator;

    tzvector();

    ~tzvector();

    bool empty() const;

    void emplace_back(const T &t);

    void clear();

    size_t size() const;

    iterator begin() const;

    iterator end() const;

    T& operator[] (const int index);

private:

    T *__data;
    size_t __size;
    size_t __capacity;

    void increase_capacity(size_t count);
};

#include "tzvector.tpp"

#endif //TZ_TZVECTOR_H
