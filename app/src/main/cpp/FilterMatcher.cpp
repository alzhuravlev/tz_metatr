//
// Created by crane on 10/21/2020.
//

#include "FilterMatcher.h"
#include "tzstring.h"

FilterMatcher::FilterMatcher() {
}

FilterMatcher::~FilterMatcher() {}

void FilterMatcher::parse(const tzstring &filter) {

    tokens.clear();

    bool inAsterisk = false;
    bool inCharSeq = false;

    tzstring currStr;

    for (const char &c : filter) {
        switch (c) {
            case '*':
                if (inCharSeq)
                    tokens.emplace_back(tzpair(FilterToken::CHAR_SEQ, currStr));
                if (!inAsterisk)
                    tokens.emplace_back(tzpair(FilterToken::ASTERISK, tzstring()));
                inAsterisk = true;
                inCharSeq = false;
                currStr.clear();
                break;

            default:
                inAsterisk = false;
                inCharSeq = true;
                currStr += c;
                break;
        }
    }
    if (inCharSeq && !currStr.empty())
        tokens.emplace_back(tzpair(FilterToken::CHAR_SEQ, currStr));
}

bool strEquals(const char *c, const char *mask) {
    size_t idx = 0;
    while (true) {
        const char *cc = &c[idx];
        const char *cmask = &mask[idx];

        if (cmask[0] == 0x0)
            return true;

        if (cc[0] == 0x0)
            return false;

        if (cmask[0] != '?' && cmask[0] != cc[0])
            return false;

        idx++;
    }
}

bool FilterMatcher::matchString(const tzstring &s) {

    if (tokens.empty())
        return false;

    bool canScanForward = false;
    tzstring scanTo;

    size_t idx = 0;

    for (const tzpair &token: tokens) {

        switch (token.token) {
            case FilterToken::ASTERISK:
                canScanForward = true;
                scanTo.clear();
                break;

            case FilterToken::CHAR_SEQ:
                scanTo = token.s;
                break;

            default:
                //noop
                break;
        }

        if (scanTo.empty())
            continue;

        size_t remainLen = s.size() - idx;

        if (scanTo.size() > remainLen)
            return false;

        if (!canScanForward) {
            bool res = strEquals(&s.c_str()[idx], scanTo.c_str());
            if (!res)
                return false;
            idx += scanTo.size();
        } else {

            canScanForward = false;

            bool found = false;

            while (idx < s.size()) {
                bool res = strEquals(&s.c_str()[idx], scanTo.c_str());
                if (res) {
                    idx += scanTo.size();
                    found = true;
                    break;
                }
                idx++;
            }

            if (!found)
                return false;
        }
    }

    if (tokens.size() > 1) {
        // if mask don't ends with asterisk...
        // then string must ends with last CHAR_SEQ
        if (!canScanForward && !scanTo.empty()) {
            const char *c = s.c_str();
            const char *mask = scanTo.c_str();
            size_t lastIdx = strlen(c) - strlen(mask);
            return strEquals(&c[lastIdx], mask);
        }
    } else {
        // if mask contains only one token and it's not an asterisk
        // then check we have finished processing whole string
        if (!canScanForward && idx < s.size())
            return false;
    }

    return true;
}

FilterMatcher::tzpair:: tzpair(FilterMatcher::FilterToken token, const tzstring & s) {
    this->token = token;
    this->s = s;
}
