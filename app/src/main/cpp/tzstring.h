//
// Created by crane on 11/5/2020.
//

#ifndef TZ_TZSTRING_H
#define TZ_TZSTRING_H

//#include <string>
//
//typedef std::string tzstring;

#include <cstddef>

class tzstring {
public:

    tzstring();

    ~tzstring();

    tzstring(const char *str, size_t l);

    tzstring(const char *str);

    tzstring(const tzstring &src);

    const char *c_str() const;

    tzstring &operator+=(char c);

    tzstring &operator=(const tzstring &rvalue);

    bool empty() const;

    void clear();

    size_t size() const;

    char *begin() const;

    char *end() const;

private:
    char *__data;
    size_t __size;
    size_t __capacity;

    void increase_capacity(size_t count);


    void copy_from(const char *src, size_t l);
};

inline tzstring operator "" _s(const char *str, size_t l) {
    return tzstring(str, l);
}

size_t strlen(const char *str);

#endif //TZ_TZSTRING_H
