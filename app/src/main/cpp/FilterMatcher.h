//
// Created by crane on 10/21/2020.
//

#ifndef TZ_FILTERMATCHER_H
#define TZ_FILTERMATCHER_H

#include "tzstring.h"
#include "tzvector.h"

using namespace std;


class FilterMatcher {
private:
    enum FilterToken {
        CHAR_SEQ,
        ASTERISK
    };

    struct tzpair {
    public:
        tzpair() = default;
        tzpair(FilterToken token, const tzstring & s);

        FilterToken token;
        tzstring s;
    };

    tzvector<tzpair> tokens;

public:
    FilterMatcher();

    ~FilterMatcher();

    void parse(const tzstring &filter);

    bool matchString(const tzstring &s);
};


#endif //TZ_FILTERMATCHER_H
