//
// Created by crane on 10/20/2020.
//

#ifndef TZ_JNI_GLOBAL_H
#define TZ_JNI_GLOBAL_H

#include "tzstring.h"

void executeCallback(const void *callbackPtr, const tzstring &s);

#endif //TZ_JNI_GLOBAL_H
