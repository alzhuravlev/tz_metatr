//
// Created by crane on 10/20/2020.
//

#include <jni.h>
#include "jni_global.h"
#include "ms_logs.h"

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env;

    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

    LOGI("Jni Init");

    return JNI_VERSION_1_6;
}
