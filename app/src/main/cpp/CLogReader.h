//
// Created by crane on 10/20/2020.
//

#ifndef TZ_CLOGREADER_H
#define TZ_CLOGREADER_H

#include "FilterMatcher.h"
#include "tzstring.h"
#include "tzvector.h"

using namespace std;

class CLogReader {
private:
    tzvector<tzstring> _results;
    tzstring _buf;

    FilterMatcher matcher;

    bool _matchString(const tzstring &s);

public:
    CLogReader();

    ~CLogReader();

    void finish();

    void consumeResults(tzvector<tzstring> &out);

    bool SetFilter(const char *filter);   // установка фильтра строк, false - ошибка
    bool AddSourceBlock(const char *block,
                        const size_t block_size); // добавление очередного блока текстового файла
};


#endif //TZ_CLOGREADER_H
