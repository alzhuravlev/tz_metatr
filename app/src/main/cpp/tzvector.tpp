//
// Created by crane on 11/5/2020.
//

template<class T>
void tzvector<T>::clear() {
    __size = 0;
}

template<class T>
size_t tzvector<T>::size() const {
    return __size;
}

template<class T>
bool tzvector<T>::empty() const {
    return begin() == end();
}

template<class T>
void tzvector<T>::emplace_back(const T &t) {
    if (__size >= __capacity - 1)
        increase_capacity(10);
    __data[__size++] = t;
}

template<class T>
typename tzvector<T>::iterator tzvector<T>::begin() const {
    return __data;
}

template<class T>
typename tzvector<T>::iterator tzvector<T>::end() const {
    return __data + __size;
}

template<class T>
tzvector<T>::tzvector() {
    __size = 0;
    __capacity = 0;
    __data = nullptr;
    increase_capacity(1);
}

template<class T>
tzvector<T>::~tzvector() {
    delete[] __data;
}

template<class T>
void tzvector<T>::increase_capacity(size_t count) {
    size_t new_capacity = __capacity + count;
    T *new_data = new T[new_capacity];
    for (size_t i = 0; i < __size; i++)
        new_data[i] = __data[i];
    delete[] __data;
    __data = new_data;
    __capacity = new_capacity;
}

template<class T>
T &tzvector<T>::operator[](const int index) {
    return __data[index];
}
