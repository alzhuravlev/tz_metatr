//
// Created by crane on 11/5/2020.
//

#include "tzstring.h"

const char *tzstring::c_str() const {
    return __data;
}

tzstring &tzstring::operator+=(char c) {
    if (__capacity < __size + 2)
        increase_capacity(10);
    __data[__size++] = c;
    __data[__size] = 0x0;
    return *this;
}

void tzstring::clear() {
    __size = 0;
    __data[0] = 0x0;
}

bool tzstring::empty() const {
    return begin() == end();
}

size_t tzstring::size() const {
    return __size;
}

tzstring::tzstring() {
    __data = nullptr;
    __capacity = 0;
    __size = 0;
    copy_from(nullptr, 0);
}

tzstring::tzstring(const char *str, size_t l) {
    __data = nullptr;
    __capacity = 0;
    __size = 0;
    copy_from(str, l);
}

tzstring::tzstring(const char *str) {
    __data = nullptr;
    __capacity = 0;
    __size = 0;
    copy_from(str, strlen(str));
}

tzstring::~tzstring() {
    delete[] __data;
}

tzstring::tzstring(const tzstring &src) {
    __data = nullptr;
    __capacity = 0;
    __size = 0;
    copy_from(src.c_str(), strlen(src.c_str()));
}

tzstring &tzstring::operator=(const tzstring &rvalue) {
    if (&rvalue != this)
        copy_from(rvalue.c_str(), strlen(rvalue.c_str()));
    return *this;
}

void tzstring::increase_capacity(size_t count) {
    size_t new_capacity = __capacity + count;
    char *new_data = new char[new_capacity];
    for (size_t i = 0; i < __size; i++)
        new_data[i] = __data[i];
    delete[] __data;
    __data = new_data;
    __capacity = new_capacity;
}

void tzstring::copy_from(const char *src, size_t l) {
    if (__capacity < l + 1)
        increase_capacity(l + 1);
    __size = l;
    for (int i = 0; i < __size; ++i)
        __data[i] = src[i];
    __data[__size] = 0x0;
}

char *tzstring::begin() const {
    return __data;
}

char *tzstring::end() const {
    return __data + __size;
}

size_t strlen(const char *str) {
    size_t size = 0;
    while (str[size] != 0x0)
        size++;
    return size;
}
