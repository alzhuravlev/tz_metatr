//
// Created by crane2002 on 5/3/2020.
//

#include  "ms_logs.h"
#include "android/log.h"

void __logi(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    __android_log_vprint(ANDROID_LOG_INFO, "fte", fmt, args);
    va_end(args);
}

void __loge(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    __android_log_vprint(ANDROID_LOG_ERROR, "fte", fmt, args);
    va_end(args);
}

void __logw(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    __android_log_vprint(ANDROID_LOG_WARN, "fte", fmt, args);
    va_end(args);
}

void __logd(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    __android_log_vprint(ANDROID_LOG_DEBUG, "fte", fmt, args);
    va_end(args);
}


