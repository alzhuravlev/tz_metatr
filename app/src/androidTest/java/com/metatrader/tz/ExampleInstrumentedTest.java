package com.metatrader.tz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Test
    public void test1() {
        LogReader logReader = new LogReader();
        logReader.setFilter("abcd");
    }

    @Test
    public void test2() {
        UrlReader urlReader = new UrlReader("https://www.ietf.org/rfc/rfc2616.txt", new UrlReader.Listener() {
            @Override
            public void onDataReceived(byte[] bytes, int len) {
                System.out.print(new String(bytes));
            }
        });
        try {
            urlReader.process();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test3() {
        LogReader logReader = new LogReader();
        logReader.setFilter("abcd");
        logReader.finish();


        UrlReader urlReader = new UrlReader("https://www.ietf.org/rfc/rfc2616.txt", new UrlReader.Listener() {
            @Override
            public void onDataReceived(byte[] bytes, int len) {
                logReader.addSourceBlock(bytes, len);
                String[] res = logReader.consumeResults();
                for (int i = 0; i < res.length; i++)
                    System.out.println(i + ". " + res[i]);
            }
        });
        try {
            urlReader.process();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test4() {
        LogReader logReader = new LogReader();

        logReader.setFilter("");
        logReader.addSourceBlockNewLine("");
        logReader.finish();
        assertArrayEquals(new String[]{}, logReader.consumeResults());

        logReader.setFilter("");
        logReader.addSourceBlockNewLine("1");
        logReader.addSourceBlockNewLine("2");
        logReader.finish();
        assertArrayEquals(new String[]{}, logReader.consumeResults());

        logReader.setFilter("*");
        logReader.addSourceBlockNewLine("1");
        logReader.addSourceBlockNewLine("2");
        logReader.finish();
        assertArrayEquals(new String[]{"1", "2"}, logReader.consumeResults());

        logReader.setFilter("?");
        logReader.addSourceBlockNewLine("1");
        logReader.addSourceBlockNewLine("2");
        logReader.addSourceBlockNewLine("12");
        logReader.addSourceBlockNewLine("");
        logReader.addSourceBlockNewLine("34");
        logReader.finish();
        assertArrayEquals(new String[]{"1", "2"}, logReader.consumeResults());

        logReader.setFilter("abc");
        logReader.addSourceBlockNewLine("abc");
        logReader.addSourceBlockNewLine("abcd");
        logReader.finish();
        assertArrayEquals(new String[]{"abc"}, logReader.consumeResults());

        logReader.setFilter("abc?");
        logReader.addSourceBlockNewLine("abc");
        logReader.addSourceBlockNewLine("abcd");
        logReader.addSourceBlockNewLine("abcde");
        logReader.finish();
        assertArrayEquals(new String[]{"abcd"}, logReader.consumeResults());

        logReader.setFilter("ab?d");
        logReader.addSourceBlockNewLine("abc");
        logReader.addSourceBlockNewLine("abcd");
        logReader.addSourceBlockNewLine("abcde");
        logReader.finish();
        assertArrayEquals(new String[]{"abcd"}, logReader.consumeResults());

        logReader.setFilter("ab?d?");
        logReader.addSourceBlockNewLine("abc");
        logReader.addSourceBlockNewLine("abcd");
        logReader.addSourceBlockNewLine("abcde");
        logReader.finish();
        assertArrayEquals(new String[]{"abcde"}, logReader.consumeResults());

        logReader.setFilter("ab?d?*");
        logReader.addSourceBlockNewLine("abc");
        logReader.addSourceBlockNewLine("abcd");
        logReader.addSourceBlockNewLine("abcde");
        logReader.addSourceBlockNewLine("abcdef");
        logReader.addSourceBlockNewLine(" abcdef");
        logReader.finish();
        assertArrayEquals(new String[]{"abcde","abcdef"}, logReader.consumeResults());

        logReader.setFilter("*ab?d?*");
        logReader.addSourceBlockNewLine("abc");
        logReader.addSourceBlockNewLine("abcd");
        logReader.addSourceBlockNewLine("abcde");
        logReader.addSourceBlockNewLine("abcdef");
        logReader.addSourceBlockNewLine(" abcdef");
        logReader.finish();
        assertArrayEquals(new String[]{"abcde","abcdef"," abcdef"}, logReader.consumeResults());

        logReader.setFilter("*a*");
        logReader.addSourceBlockNewLine("a");
        logReader.addSourceBlockNewLine(" a ");
        logReader.addSourceBlockNewLine("a ");
        logReader.addSourceBlockNewLine(" a");
        logReader.finish();
        assertArrayEquals(new String[]{"a", " a ", "a ", " a"}, logReader.consumeResults());

        logReader.setFilter("a");
        logReader.addSourceBlockNewLine("a");
        logReader.addSourceBlockNewLine(" a ");
        logReader.addSourceBlockNewLine("a ");
        logReader.addSourceBlockNewLine(" a");
        logReader.addSourceBlockNewLine("aa");
        logReader.finish();
        assertArrayEquals(new String[]{"a"}, logReader.consumeResults());

        logReader.setFilter("a*a");
        logReader.addSourceBlockNewLine("aa");
        logReader.addSourceBlockNewLine("aaa");
        logReader.addSourceBlockNewLine("a a");
        logReader.addSourceBlockNewLine("aaaa");
        logReader.addSourceBlockNewLine(" aa ");
        logReader.addSourceBlockNewLine(" aaa ");
        logReader.addSourceBlockNewLine(" aaa");
        logReader.addSourceBlockNewLine("aaa ");
        logReader.finish();
        assertArrayEquals(new String[]{"aa", "aaa", "a a", "aaaa"}, logReader.consumeResults());

        logReader.setFilter("a*aa");
        logReader.addSourceBlockNewLine("aa");
        logReader.addSourceBlockNewLine("aaa");
        logReader.addSourceBlockNewLine("a a");
        logReader.addSourceBlockNewLine("aaaa");
        logReader.addSourceBlockNewLine("aaaaaaaaaaaaaa");
        logReader.addSourceBlockNewLine(" aa ");
        logReader.addSourceBlockNewLine(" aaa ");
        logReader.addSourceBlockNewLine(" aaa");
        logReader.addSourceBlockNewLine("aaa ");
        logReader.finish();
        assertArrayEquals(new String[]{ "aaa", "aaaa", "aaaaaaaaaaaaaa"}, logReader.consumeResults());

        logReader.setFilter("*a*aa");
        logReader.addSourceBlockNewLine("aa");
        logReader.addSourceBlockNewLine("aaa");
        logReader.addSourceBlockNewLine("a a");
        logReader.addSourceBlockNewLine("aaaa");
        logReader.addSourceBlockNewLine("aaaaaaaaaaaaaa");
        logReader.addSourceBlockNewLine(" aa ");
        logReader.addSourceBlockNewLine(" aaa ");
        logReader.addSourceBlockNewLine(" aaa");
        logReader.addSourceBlockNewLine("aaa ");
        logReader.finish();
        assertArrayEquals(new String[]{ "aaa", "aaaa", "aaaaaaaaaaaaaa", " aaa"}, logReader.consumeResults());
    }
}